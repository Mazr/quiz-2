CREATE TABLE IF NOT EXISTS reservation
(
    `id`         BIGINT AUTO_INCREMENT PRIMARY KEY,
    `user_name` VARCHAR(128) NOT NULL,
    `company_name`  VARCHAR(64) NOT NULL,
    `zone_id` varchar(64) not null ,
    `start_time` datetime not null ,
    `duration` VARCHAR(16) not null

)