package com.twuc.webApp;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping("/api/timezones")
public class TimeZoneController {

    @GetMapping
    ResponseEntity getTimezones(){
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        final List<String> list = new ArrayList<>(availableZoneIds);
        Collections.sort(list);

        return ResponseEntity.ok().contentType(APPLICATION_JSON).body(list);
    }

}
