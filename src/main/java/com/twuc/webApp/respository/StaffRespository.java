package com.twuc.webApp.respository;

import com.twuc.webApp.domain.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffRespository extends JpaRepository<Staff,Long> {

}
