package com.twuc.webApp.contract;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated
public class CreateStaffRequest {
    private Long id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    public CreateStaffRequest() {
    }

    public CreateStaffRequest(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getId() {
        return id;
    }

    public CreateStaffRequest(Long id, @NotNull String firstName, @NotNull String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
