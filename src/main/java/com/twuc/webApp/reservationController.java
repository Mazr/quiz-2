package com.twuc.webApp;

import com.twuc.webApp.contract.CreateStaffRequest;
import com.twuc.webApp.contract.TimeZone;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.respository.StaffRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;

import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping("/api/staffs")
public class reservationController {
    @Autowired
    StaffRespository staffRespository;

    @PostMapping
    ResponseEntity addStaff(@Valid @RequestBody CreateStaffRequest createStaffRequest) {

        Staff staff = new Staff(createStaffRequest.getFirstName(), createStaffRequest.getLastName());

        staffRespository.save(staff);
        staffRespository.flush();
        return ResponseEntity.status(201).header("location", "/api/staffs/" + staff.getStaffId()).build();
    }

    @GetMapping("/{id}")
    private ResponseEntity getStaff(@PathVariable Long id) {
        Staff staff = staffRespository.findById(id).orElseThrow(IllegalArgumentException::new);
        return ResponseEntity.ok().contentType(APPLICATION_JSON).body(staff);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity acceExceptoionHandler(IllegalArgumentException e) {
        return ResponseEntity.status(400).build();
    }

    @GetMapping
    ResponseEntity getStaffs(){
        List<Staff> staffs = staffRespository.findAll();
        return ResponseEntity.ok().contentType(APPLICATION_JSON).body(staffs);
    }

    @PutMapping("/{staffId}/timezone")
    ResponseEntity setTimezone(@PathVariable Long staffId , @RequestBody TimeZone timeZone) {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();

        String zoneId = timeZone.getZoneId();
        boolean isContains = availableZoneIds.contains(zoneId);
        if (!isContains){
            throw new IllegalArgumentException();
        }
        Staff staff = staffRespository.findById(staffId).orElseThrow(IllegalArgumentException::new);
        staff.setZoneId(zoneId);
        staffRespository.save(staff);
        return ResponseEntity.status(200).build();
    }


}
