package com.twuc.webApp;

import com.twuc.webApp.contract.CreateStaffRequest;
import com.twuc.webApp.domain.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.zone.ZoneRulesProvider;
import java.util.*;

import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class reservationControllerTest extends ApiTestBase{
    @Test
    void should_create_staff_table() {
        
    }

    @Test
    void should_return_201_when_I_add_staff_successfully() throws Exception {

        mockMvc.perform(createStaff("Rob","Hall")).andExpect(status().is(201));

    }

    @Test
    void should_return_location_when_I_add_staff_sueecssfully() throws Exception {
        mockMvc.perform(createStaff("Rob","Hall"))
                .andExpect(header().string("location","/api/staffs/1"));

    }

    @Test
    void should_return_400_when_I_use_valided_content() throws Exception{
        mockMvc.perform(createStaff("Rob",null))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_staff_with_id() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "hhhh")).andReturn().getResponse().getHeader("location");

        mockMvc.perform(get(location))
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.firstName").value("Rob"))
                .andExpect(jsonPath("$.lastName").value("hhhh"));
        
    }

    @Test
    void should_get_400_status_code_when_I_use_no_exist_id() throws Exception {
        mockMvc.perform(get("/api/staffs/3")).andExpect(status().is(400));
    }

    @Test
    void should_get_all_staffs() throws Exception {
        mockMvc.perform(createStaff("Rob","Rooben"));
        mockMvc.perform(createStaff("Rob","Robort"));
        List<CreateStaffRequest> staffs =new ArrayList<>();
        staffs.add(new CreateStaffRequest(1L,"Rob","Rooben"));
        staffs.add(new CreateStaffRequest(2L,"Rob","Robort"));

        mockMvc.perform(get("/api/staffs"))
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(serialize(staffs)));
    }

    @Test
    void should_get_staffs_when_no_staffs() throws Exception {
        List<CreateStaffRequest> staffs = new ArrayList<>();
        mockMvc.perform(get("/api/staffs"))
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(content().json(serialize(staffs)));
    }

    @Test
    void should_return_200() throws Exception {
        
        String location = mockMvc.perform(createStaff("Rob", "hhhh")).andReturn().getResponse().getHeader("location");
        mockMvc.perform(put(location+"/timezone")
                .contentType(APPLICATION_JSON)
                .content("{\"zoneId\":\"America/Cordoba\"}")
        )
                .andExpect(status().is(200))
        ;

    }

    @Test
    void should_return_zone_id() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "hhhh")).andReturn().getResponse().getHeader("location");
        mockMvc.perform(put(location+"/timezone")
                .contentType(APPLICATION_JSON)
                .content("{\"zoneId\":\"America/Cordoba\"}")
        );
        mockMvc.perform(get(location))
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.zoneId").value("America/Cordoba"));
    }

    @Test
    void should_get_400() throws Exception {
        String location = mockMvc.perform(createStaff("Rob", "hhhh")).andReturn().getResponse().getHeader("location");
        mockMvc.perform(put(location+"/timezone")
                .contentType(APPLICATION_JSON)
                .content("{\"zoneId\":\"America/Corasdasdqweq\"}")
        )
                .andExpect(status().is(400))
        ;
    }

    @Test
    void should_return_null() throws Exception {
        String location = mockMvc.perform(createStaff("Robt", "hhhh")).andReturn().getResponse().getHeader("location");

        mockMvc.perform(get(Objects.requireNonNull(location)))
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.zoneId").isEmpty());
    }

    @Test
    void should_return_all_timezones() throws Exception {

        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        final List<String> list = new ArrayList<>(availableZoneIds);
        Collections.sort(list);
        mockMvc.perform(get("/api/timezones"))
        .andExpect(content().contentType(APPLICATION_JSON))
        .andExpect(content().string(serialize(list)));
    }

    @Test
    void name() {
    }

    private MockHttpServletRequestBuilder createStaff(String firstName, String lastName) throws Exception {
        CreateStaffRequest newStaff = new CreateStaffRequest(firstName,lastName);
        return post("/api/staffs")
                .contentType(APPLICATION_JSON)
                .content(serialize(newStaff));
    }

}